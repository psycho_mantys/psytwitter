#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GdkPixbuf, GLib, Gtk
import bleach
import requests
from joblib import Memory
from psychopy.annotations.async import Async
from psychopy.annotations.print_traceback import Print_traceback
from psychopy.annotations.cache_with_timeout import Cache_with_timeout

import os
import re
import sys
import html
from tempfile import mkdtemp
import traceback
from multiprocessing.dummy import Pool

from widget_videoplay import *
from gtkvideoplayer import Gtkvideoplayer
from resizeable_image import Resizeable_image

hashtag_re=re.compile(r'#([\w\-_]+)', re.IGNORECASE | re.MULTILINE)
at_user_re=re.compile(r'@([\w\-_]+)', re.IGNORECASE | re.MULTILINE)
linker=bleach.linkifier.Linker(callbacks=[])

def linkify(text):
	ret=linker.linkify(text)
	ret=re.sub(at_user_re,r'<a href="https://twitter.com/\1">@\1</a>',ret)
	ret=re.sub(hashtag_re,r'<a href="https://twitter.com/hashtag/\1">#\1</a>',ret)
	return ret

def text_to_html(text):
	ret=GLib.markup_escape_text(text)
	return linkify(ret)

"""
"""
filename=os.path.expanduser("~/.cache/psytwitter/")
try:
	os.makedirs(filename)
except:
	pass
mem=Memory(cachedir=filename, verbose=0)

session=requests.Session()

@Cache_with_timeout(60*60)
@mem.cache
def get_url_content(url):
	response=session.get(url)
	ret=response.content
	print(url+" "+str(response.status_code))
	return ret

def add_widget_to_container(container, item):
	print("Update container")
	container.add(item)
	item.show_all()

	return False


def update_gtkimage_from_pixbuf(gtkimage, pixbuf):
	print("Update image")
	gtkimage.set_from_pixbuf(pixbuf)
	gtkimage.show_all()

	return False


thread_pool=Pool()

@Async(thread_pool)
@Print_traceback()
def url_to_gtkimage(url, image):
	data=get_url_content(url)

	loader=GdkPixbuf.PixbufLoader()
	loader.write(data)
	loader.close()

	pixbuf=loader.get_pixbuf()

	#update_image(image, data)
	GLib.idle_add(update_gtkimage_from_pixbuf, image, pixbuf)

@Async(thread_pool)
@Print_traceback()
def handle_check_resize(container, image, pixbuf):
	print("handle_check_resize(container, image, pixbuf)")
	resized_pixbuf=x_scale_pixbuf(pixbuf, container)
	GLib.idle_add(update_gtkimage_from_pixbuf, image, pixbuf)

def pack_data_image_to_container(container, sdata):
	loader=GdkPixbuf.PixbufLoader()
	loader.write(sdata)
	loader.close()

	pixbuf=loader.get_pixbuf()

	#image=Resizeable_image(pixbuf=pixbuf, container=container.get_parent())
	#image.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
	#image.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.NEVER)
	#image.set_hexpand(True)
	#image.set_halign(Gtk.Align.FILL)
	#image.show()
	image=Gtk.Image.new_from_pixbuf(pixbuf)

	GLib.idle_add(add_widget_to_container, container, image)

def remove_childrens(w):
	w.foreach(lambda c: w.remove(c))

@Async(thread_pool)
@Print_traceback()
def url_image_to_container(url, container):
	data=get_url_content(url)
	pack_data_image_to_container(container, data)

@Async(thread_pool)
@Print_traceback()
def url_video_to_container(url, container, sizes=None):
	w=100
	h=100
	if sizes:
		if 'small' in sizes:
			w=sizes['small']['w']
			h=sizes['small']['h']
		elif 'medium' in sizes:
			w=sizes['medium']['w']
			h=sizes['medium']['h']
		elif 'large' in sizes:
			w=sizes['large']['w']
			h=sizes['large']['h']

	wvideo=Gtkvideoplayer(url, min_size=(w, h))
	wvideo.repeat(True)
	wvideo.enable_motion_reveal()
	wvideo.enable_progress()

	#GLib.idle_add(container.add, wvideo)
	GLib.idle_add(add_widget_to_container, container, wvideo)

def grab_focus(focus):
	#container.show_all()
	if focus:
		print("focus_widget.id ==> "+str(focus.id))
		focus.grab_focus()
	else:
		print("Focus is None")

def scale_pixbuf(pixbuf, parent, offset=0):
	#pixbuf=gtkimage.get_pixbuf()
	max_w=parent.get_allocation().width+offset
	max_h=parent.get_allocation().height+offset

	pixbuf_w=pixbuf.get_width()
	pixbuf_h=pixbuf.get_height()

	if (pixbuf_w>max_w) or (pixbuf_h>max_h):
		sc_factor_w=max_w/pixbuf_w
		sc_factor_h=max_h/pixbuf_h
		sc_factor=min(sc_factor_w, sc_factor_h)
		sc_w=int(pixbuf_w*sc_factor)
		sc_h=int(pixbuf_h*sc_factor)
		#scaled=pixbuf.scale_simple(sc_w, sc_h, GdkPixbuf.InterpType.HYPER)
		scaled=pixbuf.scale_simple(sc_w, sc_h, GdkPixbuf.InterpType.NEAREST)
		#gtkimage.set_from_pixbuf(scaled)
		return scaled
	return pixbuf

def y_scale_pixbuf(pixbuf, parent, offset=0):
	#pixbuf=gtkimage.get_pixbuf()
	max_h=parent.get_allocation().height+offset

	pixbuf_w=pixbuf.get_width()
	pixbuf_h=pixbuf.get_height()

	if pixbuf_h>max_h:
		sc_factor=max_h/pixbuf_h
		sc_w=int(pixbuf_w*sc_factor)
		sc_h=int(pixbuf_h*sc_factor)
		scaled=pixbuf.scale_simple(sc_w, sc_h, GdkPixbuf.InterpType.HYPER)
		return scaled
	else:
		return pixbuf

def x_scale_pixbuf(pixbuf, parent, offset=0):
	max_w=parent.get_allocation().width+offset
	#print(max_w)

	pixbuf_w=pixbuf.get_width()
	#print(pixbuf_w)
	pixbuf_h=pixbuf.get_height()
	#print(pixbuf_h)

	if pixbuf_w>max_w:
		sc_factor=max_w/pixbuf_w
		#print(sc_factor)
		sc_w=int(pixbuf_w*sc_factor)
		#print(sc_w)
		sc_h=int(pixbuf_h*sc_factor)
		#print(sc_h)
		print("scale_simple()")
		#scaled=pixbuf.scale_simple(sc_w, sc_h, GdkPixbuf.InterpType.HYPER)
		scaled=pixbuf.scale_simple(sc_w, sc_h, GdkPixbuf.InterpType.NEAREST)
		return scaled
	else:
		return pixbuf


#!/usr/bin/env python

import argparse
import os
import sys
import pickle

class Config():
	def load_cli(self):
		parser=argparse.ArgumentParser()

		parser.add_argument("--debug","-d", nargs='?', const=1, default=self.DEBUG,
			metavar='LEVEL', help="Enable debug level mode")
		parser.add_argument("--consumer-key","-k", type=str, default=self.consumer_key,
			metavar=self.consumer_key, help="Set consumer-key from twitter")
		parser.add_argument("--consumer-secret","-s", type=str, default=self.consumer_secret,
			metavar=self.consumer_secret, help="Set consumer-secret from twitter")
		parser.add_argument("--token-file","-f", type=str, default=self.token_file,
			metavar=self.token_file, help="Set file with access token")
		parser.add_argument("--load-fake-tweets-from-file","-p", type=str, default=self.load_fake_tweets_from_file,
			metavar=self.load_fake_tweets_from_file, help="Set file for load fake tweets")
		parser.add_argument("--save-fake-tweets-to-file", type=str, default=self.save_fake_tweets_to_file,
			metavar=self.save_fake_tweets_to_file, help="Set file for save fake tweets")

		args, unknown_args=parser.parse_known_args()

		self.consumer_key=args.consumer_key
		self.consumer_secret=args.consumer_secret
		self.token_file=args.token_file
		self.load_fake_tweets_from_file=args.load_fake_tweets_from_file
		self.save_fake_tweets_to_file=args.save_fake_tweets_to_file


		# Enable debug
		self.DEBUG=args.debug

		return unknown_args


	def load_sys_env(self):
		# Enable debug
		self.DEBUG=int(os.getenv('DEBUG', self.DEBUG))
		self.consumer_key=os.getenv('PSY_CONSUMER_KEY', self.consumer_key)
		self.consumer_secret=os.getenv('PSY_CONSUMER_SECRET', self.consumer_secret)
		self.token_file=os.getenv('PSY_TOKEN_FILE', self.token_file)
		self.load_fake_tweets_from_file=os.getenv('PSY_FAKE_TWEET_FILE', self.load_fake_tweets_from_file)
		self.save_fake_tweets_to_file=os.getenv('PSY_SAVE_FAKE_TWEET_FILE', self.save_fake_tweets_to_file)

	def __init__(self):
		self.consumer_key='B3U3XnY6mM4DpY00GG9c8TJoe'
		self.consumer_secret='KvMbpWeF2yzUBhmq5HAxGNMgkBIobm9croowWPp3Lu6NxIYNtC'

		self.access_key=None
		self.access_secret=None

		self.token_file="~/.psytwitter.tokens"

		self.DEBUG=0

		self.auth=None
		self.api=None
		self.load_fake_tweets_from_file=None
		self.save_fake_tweets_to_file=None

	def load_tokens(self):
		filename=os.path.expanduser(self.token_file)
		if os.path.isfile(filename):
			with open(filename, 'rb') as f:
				self.access_key, self.access_secret=pickle.load(f)

	def save_tokens(self):
		filename=os.path.expanduser(self.token_file)
		with open(filename, 'wb+') as f:
			pickle.dump((self.access_key, self.access_secret), f, pickle.HIGHEST_PROTOCOL)


#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
gi.require_version('GstVideo', '1.0')

from gi.repository import Gtk, Gst
from gi.repository import GstVideo





from gi.repository import Gdk
from gi.repository import Gst
from gi.repository import Gtk
from gi.repository import GdkX11    # for window.get_xid()
from gi.repository import GstVideo  # for sink.set_window_handle()


import sys

class Widget_videoplay(Gtk.DrawingArea):
	def __init__(self, uri):
		super().__init__()
		self.set_double_buffered(True)
		self.state=Gst.State.NULL
		self._repeat=False

		self.set_halign(Gtk.Align.START)
		self.set_valign(Gtk.Align.START)

		self.duration=Gst.CLOCK_TIME_NONE

		# take the commandline argument and ensure that it is a uri
		if Gst.uri_is_valid(uri):
			self.uri=uri
		else:
			self.uri=Gst.filename_to_uri(uri)

		self.playbin=Gst.ElementFactory.make("playbin", "playbin")
		self.playbin.set_property('uri', self.uri)

		# instruct the bus to emit signals for each received message
		# and connect to the interesting signals
		bus=self.playbin.get_bus()
		bus.add_signal_watch()
		#bus.connect('sync-message::element', self.on_sync_message)
		bus.connect("message::error", self.on_error)
		bus.connect("message::eos", self.on_eos)
		bus.connect("message::progress", self.on_progress)
		bus.connect("message::state-changed", self.on_state_changed)
		bus.connect("message::duration-changed", self.on_duration_changed)
		bus.connect("sync-message::element", self.on_sync_message)
		#bus.connect("message::application", self.on_application_message)

		self.set_double_buffered(False)
		self.connect("destroy", self.finish_player)
		self.connect("realize", self.on_realize)
		self.connect("draw", self.on_draw)

	def on_sync_message(self, bus, message):
		print(message.get_structure().get_name())
		if message.get_structure().get_name() == 'prepare-window-handle':
			imagesink=message.src
			imagesink.set_property("force-aspect-ratio", True)
			xid=self.get_property('window').get_xid()
			imagesink.set_window_handle(xid)

	def on_realize(self, widget):
		window=widget.get_window()
		window_handle=window.get_xid()

		self.playbin.set_window_handle(window_handle)


	# this function is called every time the video window needs to be
	# redrawn. GStreamer takes care of this in the PAUSED and PLAYING states.
	# in the other states we simply draw a black rectangle to avoid
	# any garbage showing up
	def on_draw(self, widget, cr):
		#states=[Gst.State.READY, Gst.State.NULL]
		#if any(state==self.state for state in states):
		if self.state<Gst.State.PAUSED:
			allocation=widget.get_allocation()

			cr.set_source_rgb(0, 0, 0)
			cr.rectangle(0, 0, allocation.width, allocation.height)
			cr.fill()

		return False

	def on_progress(self, *args):
		print(args)

	def on_state_changed(self, bus, msg):

		old, new, pending = msg.parse_state_changed()
		if not msg.src == self.playbin:
			# not from the playbin, ignore
			return
		self.state=new

	# this function is called when an error message is posted on the bus
	def on_error(self, bus, msg):
		err, dbg = msg.parse_error()
		print("ERROR:", msg.src.get_name(), ":", err.message)
		if dbg:
			print("Debug info:", dbg)

	def on_eos(self, bus, msg):
		if self._repeat:
			self.seek(0)
		else:
			self.on_stop()

	def seek(self, seconds):
		self.playbin.seek_simple(
			Gst.Format.TIME,
			Gst.SeekFlags.FLUSH|Gst.SeekFlags.ACCURATE,
			seconds*Gst.SECOND
		)
		#Gst.SeekFlags.FLUSH|Gst.SeekFlags.KEY_UNIT,

	def on_play(self, *args, **kwargs):
		self.playbin.set_state(Gst.State.PLAYING)

	def on_pause(self, *args, **kwargs):
		self.playbin.set_state(Gst.State.PAUSED)

	def on_stop(self, *args, **kwargs):
		self.playbin.set_state(Gst.State.READY)
		#self.queue_draw()

	def repeat(self, value=True):
		self._repeat=value

	def on_duration_changed(self, *args):
		self.set_duration()

	def set_duration(self):
		ret, self.duration=self.playbin.query_duration(Gst.Format.TIME)

	def finish_player(self, *args):
		if self.playbin:
			self.playbin.set_state(Gst.State.NULL)
			self.playbin=None

	def __del__(self):
		self.finish_player()

if __name__ == '__main__':


	if len(sys.argv)>1:
		URI=sys.argv[1]
	else:
		URI='https://video.twimg.com/tweet_video/DBMDLy_U0AAqUWP.mp4'

	window=Gtk.ApplicationWindow()

	widget=Widget_videoplay(URI)
	widget.set_size_request(400, 400)
	widget.repeat(True)

	box=Gtk.Box()

	overlay=Gtk.Overlay()
	overlay.show()

	#box.add(widget)
	box.add(Gtk.Label("begin"))
	box.set_halign(Gtk.Align.CENTER)
	box.set_valign(Gtk.Align.CENTER)
	box.pack_start(widget, True, True, 2)
	box.add(Gtk.Label("end"))
	box.show()



	def on_destroy(win):
		Gtk.main_quit()

	button=Gtk.Button.new_from_stock(Gtk.STOCK_OK)
	bbox=Gtk.Box()
	bbox.set_halign(Gtk.Align.CENTER)
	bbox.set_valign(Gtk.Align.CENTER)
	bbox.pack_start(button, False, False, 2)
	overlay.add_overlay(bbox)
	bbox.show_all()

	overlay.show_all()
	button.show_all()

	window.connect('destroy', on_destroy)
	overlay.add(box)
	window.add(overlay)
	#window.add(box)

	window.show_all()
	box.show_all()
	widget.show_all()
	widget.on_play()

	Gtk.main()


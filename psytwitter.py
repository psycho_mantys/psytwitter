#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, GdkPixbuf, GLib, Gdk, Gst

import tweepy
import bleach
from psychopy.annotations.async import Async
from psychopy.annotations.print_traceback import Print_traceback


from ui.windows.about import About
from ui.windows.pin import Pin
from ui.windows.popup_error import Popup_error
from ui.windows.compose import Compose
from ui.windows.show_image import Show_image

from config import Config
from static_images import get_gtkimage, get_pixbuf
import tweet_helper
import utils
import check_update
from resizeable_image import Resizeable_image

import sys
import traceback
import os
import itertools
from datetime import datetime
import html
import time
from pprint import pprint
import pickle

global_count=0

from urllib.request import urlopen


class MainWindow():

	ROOT_WINDOW = 'root_window'				# root window name
	UI_XML='ui/psytwitter.glade'			# GUI descriptor

	def __init__(self, app, config):
		self.app=app
		self.config=config
		self.builder=Gtk.Builder()
		self.builder.add_from_file(self.UI_XML)
		self.builder.connect_signals(self)
		self.root_window=self.builder.get_object(self.ROOT_WINDOW)
		self.root_window.set_application(app)
		#self.root_window.maximize()
		self.root_window.show_all()

		self.main_box=self.builder.get_object("main_box")
		self.main_scrolledwindow=self.builder.get_object("main_scrolledwindow")

		self.top_id=None
		self.bottom_id=None
		self.is_hided=False
		self.loading_tweets=False


		if not self.config.load_fake_tweets_from_file:
			self.config.auth=tweepy.OAuthHandler(
				self.config.consumer_key, self.config.consumer_secret
			)
			self.config.auth.set_access_token(
				self.config.access_key, self.config.access_secret
			)
			self.config.api=tweepy.API(self.config.auth)

			self.check_update=check_update.Check_update(self)
			self.check_update.loop_async()

		# System tray icon
		self.status_tray=Gtk.StatusIcon()
		self.status_tray.set_from_pixbuf(get_pixbuf("logo"))
		self.status_tray.connect("activate", self.toggle_hide_app)
		self.status_tray.connect("popup-menu", self.on_right_click)


		self.on_show_timeline()


	def on_right_click(self, icon, button, time):
		self.menu=Gtk.Menu()

		new_tweet=Gtk.MenuItem()
		new_tweet.set_label("New Tweet")
		new_tweet.connect("activate", self.on_compose_tweet)
		self.menu.append(new_tweet)

		quit=Gtk.MenuItem()
		quit.set_label("Quit")
		quit.connect("activate", self.on_destroy)
		self.menu.append(quit)

		self.menu.show_all()
		self.menu.popup(None, None, None, self.status_tray, button, time)

	def unhide_app(self, *args, **kwargs):
		if self.is_hided:
			self.root_window.show_all()
			self.is_hided=not self.is_hided

	def hide_app(self, *args, **kwargs):
		if not self.is_hided:
			self.root_window.hide()
			self.is_hided=not self.is_hided



	def toggle_hide_app(self, *args, **kwargs):
		if not self.is_hided:
			self.root_window.hide()
		else:
			self.root_window.show_all()
		self.is_hided=not self.is_hided

	def on_edge_reached(self, widget=None, pos=None):
		if not self.loading_tweets:
			if Gtk.PositionType.BOTTOM==pos:
				self.on_show_bottom_timeline()
			elif Gtk.PositionType.TOP==pos:
				self.on_show_top_timeline()

	def update_tweets_on_container(self, container, new_tweets, to_destroy=[], focus=None):
		print("def update_tweets_on_container(...)")

		global global_count

		print("Criando:")
		for tweet in new_tweets:
			print(str(tweet.id)+" "+str(global_count))
			nt=self.make_tweet(tweet)

			item=Gtk.ListBoxRow()
			item.id=tweet.id
			item.add(nt)

			#GLib.idle_add(utils.add_widget_to_container, container, item)
			utils.add_widget_to_container(container, item)

		print("Destruindo {}:".format(len(to_destroy)))
		for c in to_destroy:
			print(c.id)
			#GLib.idle_add(container.remove, c)
			container.remove(c)

		#GLib.idle_add(utils.grab_focus, focus)
		utils.grab_focus(focus)

		def lambida():
			self.loading_tweets=False
		GLib.idle_add(lambida)


	@Async()
	@Print_traceback()
	def on_show_top_timeline(self, widget=None, args=None):
		print("on_show_top_timeline(...)")
		if self.loading_tweets:
			return
		else:
			self.loading_tweets=True

		timeline_box=self.main_box.get_children()[0]

		childrens=timeline_box.get_children()
		childrens=sorted(childrens, key=lambda c: c.id)
		focus=childrens[-1]

		public_tweets=None
		if self.config.load_fake_tweets_from_file and os.path.isfile(self.config.load_fake_tweets_from_file+".top"):
			with open(self.config.load_fake_tweets_from_file+".top", 'rb') as f:
				print("Load fake data...")
				public_tweets=pickle.load(f)
		else:
			public_tweets=self.config.api.home_timeline(
				tweet_mode='extended',
				since_id=self.top_id+1,
				count=15
			)
			if self.config.save_fake_tweets_to_file and os.path.isfile(self.config.save_fake_tweets_to_file+".top"):
				with open(self.config.save_fake_tweets_to_file+".top", 'wb') as f:
					pickle.dump(public_tweets, f, pickle.HIGHEST_PROTOCOL)



		new_tweets=public_tweets

		childrens=itertools.islice(childrens, len(new_tweets))

		to_destroy=list(childrens)
		self.bottom_id=to_destroy[-1].id

		self.update_tweets_on_container(timeline_box, public_tweets, to_destroy, focus)



	@Async()
	@Print_traceback()
	def on_show_bottom_timeline(self, args=None):
		print("on_show_bottom_timeline(...)")
		if self.loading_tweets:
			return
		else:
			self.loading_tweets=True

		public_tweets=None
		if self.config.load_fake_tweets_from_file and os.path.isfile(self.config.load_fake_tweets_from_file+".bottom"):
			with open(self.config.load_fake_tweets_from_file+".bottom", 'rb') as f:
				print("Load fake data...")
				public_tweets=pickle.load(f)
		else:
			public_tweets=self.config.api.home_timeline(
				tweet_mode='extended',
				max_id=self.bottom_id,
				count=15
			)
			if self.config.save_fake_tweets_to_file and os.path.isfile(self.config.save_fake_tweets_to_file+".bottom"):
				with open(self.config.save_fake_tweets_to_file+".bottom", 'wb') as f:
					pickle.dump(public_tweets, f, pickle.HIGHEST_PROTOCOL)


		def add_and_remove():
			timeline_box=self.main_box.get_children()[0]

			childrens=timeline_box.get_children()
			childrens=sorted(childrens, key=lambda c: c.id)

			focus=childrens[0]
			#focus=None

			print("Criando:")
			new_tweets=public_tweets[1:]

			self.bottom_id=new_tweets[-1].id

			childrens=reversed(childrens)
			childrens=itertools.islice(childrens, len(new_tweets))

			to_destroy=list(childrens)

			self.top_id=to_destroy[-1].id

			self.update_tweets_on_container(timeline_box, public_tweets, to_destroy, focus)

		GLib.idle_add(add_and_remove)


		#self.update_tweets_on_container(timeline_box, public_tweets, [], focus)


	def on_show_like(self, args=None):
		Popup_error("Like tab now working implemented!", self)
		pass
	def on_show_direct_message(self, args=None):
		Popup_error("Show direct message tab now working implemented!", self)
		pass
	def on_show_mention(self, args=None):
		Popup_error("Show mentions tab now working implemented!", self)
		pass


	@Async()
	@Print_traceback()
	def on_show_timeline(self, args=None):
		print("on_show_timeline()")

		if self.loading_tweets:
			return
		else:
			self.loading_tweets=True

		public_tweets=None
		if self.config.load_fake_tweets_from_file and os.path.isfile(self.config.load_fake_tweets_from_file):
			with open(self.config.load_fake_tweets_from_file, 'rb') as f:
				print("Load fake data...")
				public_tweets=pickle.load(f)
		else:
			public_tweets=self.config.api.home_timeline(
				tweet_mode='extended',
				count=40
			)
			if self.config.save_fake_tweets_to_file and os.path.isfile(self.config.save_fake_tweets_to_file):
				with open(self.config.save_fake_tweets_to_file, 'wb') as f:
					pickle.dump(public_tweets, f, pickle.HIGHEST_PROTOCOL)

		def add_and_remove():

			self.top_id=public_tweets[0].id
			self.bottom_id=public_tweets[-1].id

			#GLib.idle_add(utils.remove_childrens, self.main_box)
			utils.remove_childrens(self.main_box)

			timeline_box=Gtk.ListBox()
			#def add_list_box():
			timeline_box.set_sort_func(tweet_helper.compare)
			self.main_box.add(timeline_box)
			timeline_box.show_all()
			#GLib.idle_add(add_list_box)

			self.update_tweets_on_container(timeline_box, public_tweets)

		GLib.idle_add(add_and_remove)

	def on_launch_test2(self, args=None):
		uri='http://pbs.twimg.com/media/Di8VDUMUcAEBAOq.png'
		Show_image(self.app, uri)

	def on_launch_test(self, args=None):

		builder=Gtk.Builder()
		builder.add_from_file("ui/tweet_box.glade")
		builder.connect_signals(self)
		tid=1012460345888104448
		tid=1019761600876687360
		# Video tweet
		tid=1024085773128282112
		t=self.config.api.get_status(tid)
		#tweet_box=None
		print(t)
		tweet_box=self.make_tweet(t)

		Compose(self.app, self.config, tweet_box=tweet_box)
		pass


	def on_compose_tweet(self, args=None):
		Compose(self.app, self.config)

	def on_get_pin(self, args=None):
		Pin(self.app, self.config)

	def make_tweet(self, tweet):
		global global_count
		global_count+=1
		builder=Gtk.Builder()
		builder.add_from_file("ui/tweet_box.glade")
		builder.connect_signals(self)
	
		# Quote Button
		quote_bt=builder.get_object("quote_button")
		quote_bt.set_image(get_gtkimage("quote"))
		def on_quote_clicked(button, id):
			builder=Gtk.Builder()
			builder.add_from_file("ui/tweet_box.glade")
			builder.connect_signals(self)
			t=self.config.api.get_status(id)
			tweet_box=self.make_tweet(t)

			quote_to="https://twitter.com/{screen_name}/status/{id}".format(
				id=tweet.id,
				screen_name=tweet.user.screen_name
			)

			Compose(
				self.app, self.config,
				tweet_box=tweet_box,
				quote_to=quote_to
			)
		quote_bt.connect("clicked", on_quote_clicked, tweet.id)

		# Like Button
		like_bt=builder.get_object("like_button")
		if tweet.favorited:
			like_bt.set_image(get_gtkimage("liked"))
		else:
			like_bt.set_image(get_gtkimage("like"))
		def on_like_clicked(button, id):
			api=self.config.api
			t=api.get_status(id)
			if t.favorited:
				api.destroy_favorite(id)
				button.set_image(get_gtkimage("like"))
			else:
				api.create_favorite(id)
				button.set_image(get_gtkimage("liked"))
		like_bt.connect("clicked", on_like_clicked, tweet.id)

		# Retweet Button
		rt_bt=builder.get_object("retweet_button")
		if tweet.retweeted:
			rt_bt.set_image(get_gtkimage("retweeted"))
		else:
			rt_bt.set_image(get_gtkimage("retweet"))
		def on_rt_clicked(button, id):
			t=self.config.api.get_status(id)
			api=self.config.api
			if t.retweeted:
				api.unretweet(t.id)
				button.set_image(get_gtkimage("retweet"))
			else:
				api.retweet(id)
				button.set_image(get_gtkimage("retweeted"))
		rt_bt.connect("clicked", on_rt_clicked, tweet.id)
		
		# Retweet Button
		talk_bt=builder.get_object("talk_button")
		talk_bt.set_image(get_gtkimage("talk"))
		talk_bt.connect("clicked", self.on_talk_clicked, tweet.id)

		# Set retweet status
		if hasattr(tweet, 'retweeted_status'):
			header=builder.get_object("status_box")
			l=Gtk.Label()
			l.set_markup(
				'<small><a href="https://twitter.com/'+tweet.user.screen_name+'">'+html.escape(tweet.user.name)+'</a></small>'
			)
			i=get_gtkimage("retweeted-small")
			header.pack_start(i, expand=False, fill=False, padding=5)
			header.pack_start(l, expand=False, fill=False, padding=5)
			tweet_helper.set_widget(builder, tweet.retweeted_status)

			entities=tweet_helper.get_entities(tweet.retweeted_status)
		elif hasattr(tweet, 'in_reply_to_status_id') and tweet.in_reply_to_status_id:
			in_reply_to_screen_name=tweet.in_reply_to_screen_name
			in_reply_to_status_id=tweet.in_reply_to_status_id

			header=builder.get_object("status_box")
			l=Gtk.Label()
			#l.set_markup("<small>"+utils.text_to_html('@'+str(in_reply_to_screen_name))+"</small>")
			l.set_markup(
				'<a href="https://twitter.com/{screen_name}/status/{id}">{TEXT}</a>'.format(
					screen_name=in_reply_to_screen_name,
					id=in_reply_to_status_id,
					TEXT="@"+in_reply_to_screen_name
			))

			i=get_gtkimage("talk-small")
			header.pack_start(i, expand=False, fill=False, padding=5)
			header.pack_start(l, expand=False, fill=False, padding=5)
			tweet_helper.set_widget(builder, tweet)

			entities=tweet_helper.get_entities(tweet)
		else:
			tweet_helper.set_widget(builder, tweet)

			entities=tweet_helper.get_entities(tweet)

		# Quoted:
		quoted_status=None
		if hasattr(tweet, 'quoted_status') and tweet.is_quote_status:
			quoted_status=tweet.quoted_status
		elif	hasattr(tweet, 'retweeted_status') \
				and hasattr(tweet.retweeted_status, 'quoted_status') \
				and tweet.retweeted_status.is_quote_status:

			quoted_status=tweet.retweeted_status.quoted_status

		if quoted_status:
			builder_quote=Gtk.Builder()
			builder_quote.add_from_file("ui/small_tweet_box.glade")
			builder_quote.connect_signals(self)

			quote_box=builder.get_object("quote_box")
			tweet_helper.set_widget(builder_quote, quoted_status)

			quote_box.pack_start(builder_quote.get_object("small_tweet_box"), expand=False, fill=False, padding=5)

			entities=tweet_helper.get_entities(quoted_status)

		#global global_count
		#builder.get_object("tweet_time").set_text(str(tweet.id)+" "+str(global_count))


		media_box=builder.get_object("media_box")

		flowbox=Gtk.FlowBox()
		flowbox.set_activate_on_single_click(False)
		#flowbox.set_valign(Gtk.Align.START)
		flowbox.set_halign(Gtk.Align.FILL)
		flowbox.set_halign(Gtk.Align.CENTER)
		flowbox.set_hexpand(True)
		def show_image(fb, fbc):
			Show_image(self.app, fbc.get_child().uri)
		flowbox.connect("child-activated", show_image)
		#flowbox.set_max_children_per_line(4)
		#flowbox.set_selection_mode(Gtk.SelectionMode.NONE)
		media_box.add(flowbox)

		for media in entities.get("media", []):
			if media.get('type')=="photo":
				print("Photo: "+media['media_url'])

				image=Resizeable_image(uri=media["media_url"], container=flowbox.get_parent())
				image.show_all()

				GLib.idle_add(flowbox.add, image)
			elif media.get('type')=="animated_gif":
				print("Animated_gif: "+str(media))
				utils.url_video_to_container(media['video_info']['variants'][0]["url"], flowbox, media["sizes"])
			elif media.get('type')=="video":
				print("Video: "+str(media))
				utils.url_video_to_container(media['video_info']['variants'][0]["url"], flowbox, media["sizes"])

		ret=builder.get_object("tweet_box")
		ret.id=tweet.id
		ret.show_all()
		return ret


	def on_talk_clicked(self, button, id):
		builder=Gtk.Builder()
		builder.add_from_file("ui/tweet_box.glade")
		builder.connect_signals(self)
		t=self.config.api.get_status(id)
		tweet_box=self.make_tweet(t)

		Compose(self.app, self.config,
			tweet_box=tweet_box,
			in_reply_to_id=id,
			in_reply_to_screen_name=t.user.screen_name
		)
		pass


	def on_destroy(self,window):
		"Terminate program at window destroy"
		self.app.quit()
		#self.root_window.destroy()
		#Gtk.main_quit()

	def on_about(self, dump):
		About(self)


class MyApplication(Gtk.Application):
	main_window=None
	def __init__(self, config):
		self.config=config
		Gtk.Application.__init__(self)

	def do_activate(self):
		self.main_window=MainWindow(self, self.config)

	def do_startup(self):
		Gtk.Application.do_startup(self)


if __name__ == '__main__':
	config=Config()

	config.load_sys_env()
	unknown_args=config.load_cli()

	config.load_tokens()

	app=MyApplication(config)
	exit_status=app.run(unknown_args)
	sys.exit(exit_status)


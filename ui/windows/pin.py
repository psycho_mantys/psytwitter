#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf

import bleach

from auth import *

class Pin():

	ROOT_WINDOW = 'root_window'				# root window name
	UI_XML='ui/windows/pin.glade'				# GUI descriptor

	def __init__(self, app, config):
		self.config=config
		self.app=app
		self.builder=Gtk.Builder()
		self.builder.add_from_file(self.UI_XML)
		self.builder.connect_signals(self)
		self.root_window=self.builder.get_object(self.ROOT_WINDOW)
		self.root_window.set_application(app)

		self.url_label=self.builder.get_object("url_label")

		url=get_pin_url(config.auth)
		linker=bleach.linkifier.Linker(callbacks=[])
		self.url_label.set_markup("<b>"+linker.linkify(url)+"</b>")

		self.pin_box=self.builder.get_object("pin_box")

		self.root_window.show_all()

	def on_cancel_button_clicked(self, button=None):
		self.on_destroy()

	def on_ok_button_clicked(self, button=None):
		pin=self.pin_box.get_text()
		(self.config.access_key, self.config.access_secret)=access_from_pin(
			self.config.auth, pin
		)
		self.config.save_tokens()

		self.on_destroy()

	def on_destroy(self):
		self.root_window.destroy()


#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class About():
	def __init__(self, editor):
		# lists of authors and documenters (will be used later)
		self.authors = ["Psycho Mantys"]
		self.documenters = []

		self.app=editor.app
		self.parent_window=editor.root_window
		self.editor=editor

		self.root_window=Gtk.AboutDialog()
		self.root_window.set_application(self.app)
		self.root_window.set_transient_for(self.parent_window)


		# we fill in the aboutdialog
		self.root_window.set_program_name("psytwitter")
		self.root_window.set_copyright("Copyright \xc2\xa9 2018 Psycho Mantys")
		self.root_window.set_authors(self.authors)
		#self.root_window.set_documenters(documenters)
		self.root_window.set_website("https://bitbucket.org/psycho_mantys/psytwitter")
		self.root_window.set_website_label("BitBucket Repository")
#		self.root_window.set_logo(Gtk.image_new_form_stock(Gtk.STOCK_DIALOG_AUTHENTICATION, Gtk.ICON_SIZE_MENU))
		self.root_window.set_logo(self.root_window.render_icon(Gtk.STOCK_ABOUT, Gtk.IconSize.DIALOG))

		# we do not want to show the title, which by default would be "About AboutDialog Example"
		# we have to reset the title of the messagedialog window after setting
		# the program name
		self.root_window.set_title("")

		self.root_window.set_transient_for(self.parent_window)
		self.root_window.set_modal(True)

		# to close the aboutdialog when "close" is clicked we connect the
		# "response" signal to on_close
		self.root_window.connect("response", lambda x,y: self.root_window.destroy())

#		self.root_window.maximize()
		self.root_window.show_all()

	def on_about(self, dump):
		# a  Gtk.AboutDialog
		pass


#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GdkPixbuf, GLib


import utils


class Show_image(Gtk.Window):
	def __init__(self, app, uri):
		super().__init__()
		self.set_application(app)
	
		sw=Gtk.ScrolledWindow()
		self.add(sw)

		utils.url_image_to_container(uri, sw)

		self.connect('destroy', self.on_destroy)

		self.show_all()
		self.maximize()

	def on_destroy(self, *args):
		self.destroy()


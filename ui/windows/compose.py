#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf, GLib

import bleach

import tweet_helper

class Compose():

	ROOT_WINDOW='root_window'				# root window name
	UI_XML='ui/windows/compose.glade'				# GUI descriptor

	def __init__(self, app, config, **kwargs):
		self.config=config
		self.app=app
		self.builder=Gtk.Builder()
		self.builder.add_from_file(self.UI_XML)
		self.builder.connect_signals(self)
		self.root_window=self.builder.get_object(self.ROOT_WINDOW)
		self.root_window.set_application(app)

		self.root_window.show_all()

		self.tweet_text=self.builder.get_object("tweet_text")
		self.compose_label=self.builder.get_object("compose_label")
		self.tweet_box=self.builder.get_object("tweet_box")

		self.in_reply_to_screen_name=None
		self.in_reply_to_id=None
		self.quote_to=None

		if "in_reply_to_id" in kwargs:
			self.in_reply_to_id=kwargs["in_reply_to_id"]
			self.in_reply_to_screen_name=kwargs["in_reply_to_screen_name"]
			self.compose_label.set_markup("<big>Reply a tweet:</big>")
			wbuffer=self.tweet_text.get_buffer()
			wbuffer.set_text("@"+self.in_reply_to_screen_name+" ")
		elif "quote_to" in kwargs:
			self.quote_to=kwargs["quote_to"]

		if ("label_text" in kwargs) and self.compose_label:
			self.compose_label.set_text(kwargs["label_text"])

		if "tweet_box" in kwargs and kwargs["tweet_box"]:
			print(kwargs["tweet_box"])
			print(self.tweet_box)
			self.tweet_box.add(kwargs["tweet_box"])

	def on_cancel_button_clicked(self, button=None):
		self.on_destroy()

	def on_tweet(self, button=None):
		wbuffer=self.tweet_text.get_buffer()
		text=wbuffer.get_text(wbuffer.get_start_iter(), wbuffer.get_end_iter(), True)
		print(text)

		if self.in_reply_to_id:
			self.config.api.update_status(text, in_reply_to_status_id=self.in_reply_to_id)
		elif self.quote_to:
			self.config.api.update_status(status=text+self.quote_to)
		else:
			self.config.api.update_status(status=text)

		self.on_destroy()

	def on_destroy(self):
		self.root_window.destroy()


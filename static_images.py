#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf

CACHE_IMAGE={}
CACHE_IMAGE["retweeted"]=GdkPixbuf.Pixbuf.new_from_file("media/retweeted.png")
CACHE_IMAGE["retweeted-small"]=GdkPixbuf.Pixbuf.new_from_file("media/retweeted-small.png")
CACHE_IMAGE["retweet"]=GdkPixbuf.Pixbuf.new_from_file("media/retweet.png")
CACHE_IMAGE["liked"]=GdkPixbuf.Pixbuf.new_from_file("media/liked.png")
CACHE_IMAGE["like"]=GdkPixbuf.Pixbuf.new_from_file("media/like.png")
CACHE_IMAGE["talk"]=GdkPixbuf.Pixbuf.new_from_file("media/talk.png")
CACHE_IMAGE["talk-small"]=GdkPixbuf.Pixbuf.new_from_file("media/talk-small.png")
CACHE_IMAGE["quote"]=GdkPixbuf.Pixbuf.new_from_file("media/quote.png")
CACHE_IMAGE["logo"]=GdkPixbuf.Pixbuf.new_from_file("media/logo.png")

def get_gtkimage(image_id):
	return Gtk.Image.new_from_pixbuf(CACHE_IMAGE[image_id])
	#return Gtk.Image.new_from_file(image_id)

def get_pixbuf(image_id):
	return CACHE_IMAGE["logo"]


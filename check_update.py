#!/usr/bin/env python


import gi
gi.require_version('Notify', '0.7')
gi.require_version('Gtk', '3.0')
from gi.repository import Notify, Gtk, GLib


import os.path
import time
import sys
import traceback

import static_images
from psychopy.annotations.async import Async
from psychopy.annotations.print_traceback import Print_traceback

Notify.init("psytwitter")

class Check_update(object):
	def __init__(self, app):
		self.app=app

		self.last={}
		self.last["timeline"]=self.app.config.api.home_timeline(count=1)[0].id
		self.last["direct_message"]=self.app.config.api.direct_messages(count=1)[0].id
		self.last["retweeted"]=self.app.config.api.retweets_of_me(count=1)[0].id
		

		self.query_time=10*60
		self._end=False


	def notify(self, tweets, new_text):
		try:
			tweet=tweets[0]
			# A raw file name/path
			notification=Notify.Notification.new(
				"New "+new_text,
				"You have "+str(len(tweets))+" "+new_text
			)
			image=static_images.get_pixbuf("logo")
			notification.set_icon_from_pixbuf(image)
			notification.set_image_from_pixbuf(image)

			notification.add_action(
				"action_click",
				"Open psytwitter",
				self.app.unhide_app,
				None
			)
			#GLib.idle_add(notification.show)
			notification.show()
		except:
			traceback.print_exc(file=sys.stdout)
			raise


	def finalize(self):
		self._end=True

	def loop(self):
		while not self._end:
			time.sleep(self.query_time)
			self.verify_new_tweet()
			self.verify_new_retweet()
			self.verify_new_dm()

	@Async()
	@Print_traceback()
	def loop_async(self):
		try:
			self.loop()
		except:
			traceback.print_exc(file=sys.stdout)
			raise
			
	def verify_new_dm(self):
		dm=self.app.config.api.direct_messages(
			since_id=self.last["direct_message"]
		)
		if len(dm):
			self.last["direct_message"]=dm[0].id
			GLib.idle_add(self.notify, dm, "Direct Message")

	def verify_new_tweet(self):
		public_tweets=self.app.config.api.home_timeline(
			since_id=self.last["timeline"]
		)
		if len(public_tweets):
			self.last["timeline"]=public_tweets[0].id
			GLib.idle_add(self.notify, public_tweets, "tweet")

	def verify_new_retweet(self):
		retweets=self.app.config.api.retweets_of_me(
			since_id=self.last["retweeted"]
		)
		if len(retweets):
			self.last["retweeted"]=retweets[0].id
			GLib.idle_add(self.notify, retweets, "retweet")



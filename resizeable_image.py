#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GdkPixbuf, GLib
from psychopy.annotations.async import Async
from psychopy.annotations.print_traceback import Print_traceback


import sys


import utils


#class Resizeable_image(Gtk.ScrolledWindow):
class Resizeable_image(Gtk.Image):
	def __init__(self, **kwargs):
		super().__init__()

		self.min_size=kwargs.pop("min_size", None)
		self.uri=kwargs.pop("uri", None)
		pixbuf=kwargs.pop("pixbuf", None)
		self.container=kwargs["container"]

		if self.uri:
			if self.uri.startswith("http://") or self.uri.startswith("https://"):
				data=utils.get_url_content(self.uri)

				loader=GdkPixbuf.PixbufLoader()
				loader.write(data)
				loader.close()
				
				self.pixbuf=loader.get_pixbuf()
			else:
				self.pixbuf=GdkPixbuf.Pixbuf.new_from_file(self.uri)
		elif pixbuf:
			self.pixbuf=pixbuf
		else:
			raise RuntimeError("Pixbuf cannot created by args")

		self.resized_pixbuf=self.pixbuf
		self.set_from_pixbuf(self.pixbuf)

		if self.min_size:
			self.set_size_request(self.min_size[0], self.min_size[1])

		self.vadjustment=self.container.get_vadjustment()
		self.hadjustment=self.container.get_hadjustment()

		self.vadjustment_handler_id=self.vadjustment.connect("changed", self.resize_image)
		self.hadjustment_handler_id=self.hadjustment.connect("changed", self.resize_image)

	def on_parent_set(self, *args):

#		if widget==self:
#			return

		self.vadjustment=self.get_parent().get_vadjustment()
		self.hadjustment=self.get_parent().get_hadjustment()

		self.vadjustment_handler_id=self.vadjustment.connect("changed", self.resize_image)
		self.hadjustment_handler_id=self.hadjustment.connect("changed", self.resize_image)


	def resize_image(self, *args):
		#@Async()
		#@Print_traceback()
		def rs():
			print("rs():")
			self.vadjustment.handler_block(self.vadjustment_handler_id)
			self.hadjustment.handler_block(self.hadjustment_handler_id)

			if self.is_resizeable(self.container):
				new_pixbuf=utils.x_scale_pixbuf(self.pixbuf, self.container, -4)

				if new_pixbuf and not is_equal_size(self.resized_pixbuf, new_pixbuf):
					print("Scale image")
					self.resized_pixbuf=new_pixbuf
					self.set_from_pixbuf(self.resized_pixbuf)
			else:
				self.set_from_pixbuf(self.pixbuf)
				self.resized_pixbuf=self.pixbuf

			self.vadjustment.handler_unblock(self.vadjustment_handler_id)
			self.hadjustment.handler_unblock(self.hadjustment_handler_id)

		#GLib.idle_add(rs)
		rs()

	def is_resizeable(self, parent):
		max_w=parent.get_allocation().width
		max_h=parent.get_allocation().height

		pb_w=self.pixbuf.get_width()
		pb_h=self.pixbuf.get_height()

		#rpb_w=self.resized_pixbuf.get_width()
		#rpb_h=self.resized_pixbuf.get_height()

		return ( (pb_w>=max_w) or (pb_h>=max_h) )
	
def is_equal_size(pb1, pb2):
	#print("1 "+str(pb1))
	#print("2 "+str(pb2))
	return (pb1.get_width() == pb2.get_width()) and (pb1.get_height() == pb2.get_height())

if __name__ == '__main__':
	if len(sys.argv)>1:
		image=sys.argv[1]
	else:
		image='https://video.twimg.com/tweet_video/DBMDLy_U0AAqUWP.mp4'

	window=Gtk.ApplicationWindow()

	sw=Gtk.ScrolledWindow()

	widget=Resizeable_image(uri=image)
	#widget=Resizeable_image(uri=image, min_size=(100,100))

	#widget.set_vexpand(True)
	#widget.set_hexpand(True)
	#widget.set_halign(Gtk.Align.FILL)
	#widget.set_valign(Gtk.Align.FILL)

	def on_destroy(win):
		Gtk.main_quit()

	window.connect('destroy', on_destroy)

	#widget.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
	sw.add(widget)

	#sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.NEVER)
	window.add(sw)

	window.show_all()

	Gtk.main()


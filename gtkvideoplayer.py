#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
gi.require_version('GstVideo', '1.0')

from gi.repository import Gtk, Gst, GLib
from gi.repository import GstVideo
from gi.repository import Gdk
from gi.repository import Gst
from gi.repository import Gtk
from gi.repository import GdkX11    # for window.get_xid()
from gi.repository import GstVideo  # for sink.set_window_handle()

import sys

from widget_videoplay import *

Gst.init()

class Gtkvideoplayer(Gtk.Overlay):
	def __init__(self, URI, **kwargs):
		super().__init__()


		# Miliseconds to hide
		self.activity_timeout=5000
		self.slider_timeout=150

		min_size=kwargs.pop("min_size", (100,100))

		self.builder=Gtk.Builder()
		self.builder.add_from_file("ui/video_box.glade")
		self.builder.connect_signals(self)

		self.videoplay=Widget_videoplay(URI)

		self.videoplay.set_size_request(min_size[0], min_size[1])

		self.videoplay.set_halign(Gtk.Align.FILL)
		self.videoplay.set_valign(Gtk.Align.FILL)

		self.show_controls=None

		self.control_reveal=self.builder.get_object("control_reveal")
		self.duration_slider=self.builder.get_object("duration_slider")

		#self.control_reveal.set_halign(Gtk.Align.CENTER)
		self.control_reveal.set_halign(Gtk.Align.FILL)
		#self.control_reveal.set_valign(Gtk.Align.CENTER)
		self.control_reveal.set_valign(Gtk.Align.END)
		#self.control_reveal.set_valign(Gtk.Align.FILL)

		self.add_overlay(self.control_reveal)
		self.add(self.videoplay)

		self.control_reveal.set_reveal_child(True)

		self.slider_update_signal_id=self.duration_slider.connect("value-changed", self.on_duration_slider_changed)
		self.connect("realize", self.on_realize)

	def enable_motion_reveal(self, *args):
		self.videoplay.set_events(Gdk.EventMask.POINTER_MOTION_MASK)
		self.videoplay.connect("motion-notify-event", self.on_actitivity)
		#self.videoplay.connect("move-focus", self.on_actitivity)
		self.on_actitivity()

	def on_realize(self, widget):
		self.videoplay.on_play()
		self.videoplay.on_pause()

	def on_duration_slider_changed(self, range):
		value=self.duration_slider.get_value()
		self.videoplay.seek(value)
	
	def on_set_duration_slider(self):
		ret, current=self.videoplay.playbin.query_position(Gst.Format.TIME)
		self.duration_slider.set_value(current/Gst.SECOND)

	def set_range_duration_slider(self, *args):
		self.duration_slider.set_range(0, self.videoplay.duration/Gst.SECOND)

	def on_stop(self, *args):
		self.videoplay.on_stop()

	def on_play(self, *args):
		self.videoplay.on_play()
		self.videoplay.set_duration()
		self.set_range_duration_slider()

	def on_pause(self, *args):
		self.videoplay.on_pause()

	def repeat(self, value=True):
		self.videoplay.repeat(value)

	def on_actitivity(self, *args):
		self.control_reveal.show_all()

		def show_control():
			self.control_reveal.hide()
			self.show_controls=None

		if self.show_controls:
			GLib.source_remove(self.show_controls)

		self.show_controls=GLib.timeout_add(self.activity_timeout, show_control)

	def enable_progress(self):
		def set_progress():
			if self.slider_update_signal_id:
				if not self.videoplay.playbin:
					return

				ret, current=self.videoplay.playbin.query_position(Gst.Format.TIME)

				self.duration_slider.handler_block(self.slider_update_signal_id)

				self.set_range_duration_slider()
				self.on_set_duration_slider()

				self.duration_slider.handler_unblock(self.slider_update_signal_id)

				GLib.timeout_add(self.slider_timeout, set_progress)

		GLib.timeout_add(self.slider_timeout, set_progress)



if __name__ == '__main__':
	if len(sys.argv)>1:
		URI=sys.argv[1]
	else:
		URI='https://video.twimg.com/tweet_video/DBMDLy_U0AAqUWP.mp4'

	window=Gtk.ApplicationWindow()

	widget=Gtkvideoplayer(URI, min_size=(400,400))

	widget.repeat(True)
	widget.enable_motion_reveal()
	widget.enable_progress()
	widget.on_play()

	def on_destroy(win):
		Gtk.main_quit()

	window.connect('destroy', on_destroy)
	window.add(widget)

	window.show_all()

	Gtk.main()


#!/usr/bin/env python

import utils
import html

def set_widget(builder, tweet):
	text=get_text(tweet)
	user=tweet.user.screen_name

	#builder.get_object("tweet_user").set_markup('<a href="https://twitter.com/'+user+'">@'+user+'</a>')
	builder.get_object("tweet_user").set_markup(utils.text_to_html("@"+user))
	t_s=builder.get_object("source_url")
	if t_s:
		t_s.set_markup(get_url(tweet))
	builder.get_object("tweet_user_fullname").set_markup(
		'<big><a href="https://twitter.com/'+user+'">'+html.escape(tweet.user.name)+'</a></big>'
	)
	tweet_time=tweet.created_at
	builder.get_object("tweet_time").set_markup(
		get_url(tweet, "{YEAR}/{MONTH:02d}/{DAY:02d} {HOUR:02d}:{MIN:02d}".format(
			YEAR=tweet_time.year,
			MONTH=tweet_time.month,
			DAY=tweet_time.day,
			HOUR=tweet_time.hour,
			MIN=tweet_time.minute
	)))

	utils.url_to_gtkimage(
		tweet.user.profile_image_url,
		builder.get_object("tweet_user_photo")
	)
	builder.get_object("tweet_text").set_markup(utils.linkify(text))
	#print("ID::{} TEXT::{}".format(tweet.id, tweet_text_to_html(text)))

def get_url(tweet, text="Source"):
	return '<a href="https://twitter.com/{screen_name}/status/{id}">{TEXT}</a>'.format(
		screen_name=tweet.user.screen_name,
		id=tweet.id,
		TEXT=html.escape(text)
	)

def get_text(tweet):
	try:
		tweet_text=tweet.full_text
	except AttributeError:
		tweet_text=tweet.text

	return tweet_text

def get_entities(tweet):
	try:
		ret=tweet.extended_entities
	except AttributeError:
		ret=tweet.entities

	return ret

def compare(tweet_1, tweet_2, *args, **kwargs):
	#print(tweet_1)
	#print(tweet_2)
	#print(tweet_1.id)
	#print(tweet_2.id)
	return tweet_1.id<tweet_2.id


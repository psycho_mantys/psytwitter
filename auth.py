#!/usr/bin/env python

def get_auth(consumer_key, consumer_secret):
	return tweepy.OAuthHandler(consumer_key, consumer_secret)

def access_from_pin(auth, pin):
	auth.get_access_token(pin)
	return auth.access_token, auth.access_token_secret

def get_pin_url(auth):
	return auth.get_authorization_url()

